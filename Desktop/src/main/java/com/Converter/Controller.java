package com.Converter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private Button lengthButton;

    @FXML
    private Button temperatureButton;

    @FXML
    private Button volumeButton;

    @FXML
    private Button weightButton;

    @FXML
    private Button timeButton;

    @FXML
    private Button calculateButton;

    @FXML
    private TextField textFirstNumber;

    @FXML
    private TextField textSecondNumber;

    @FXML
    private ComboBox<String> selectFirst;

    @FXML
    private ComboBox<String> selectSecond;

    @FXML
    void handleButtonAction(ActionEvent event) {

        selectFirst.getItems().removeAll("m", "C", "l", "sec", "kg");
        selectSecond.getItems().removeAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra",
                "N", "D", "m^3", "gallon", "pint", "quart", "barrel",
                "cubic foot", "cubic inch", "g", "c", "carat", "eng pound",
                "pound", "stone", "rus pound");
        selectFirst.getItems().addAll("m");
        selectSecond.getItems().addAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard");
    }

    @FXML
    void handleButtonAction2(ActionEvent event) {
        selectFirst.getItems().removeAll("m", "C", "l", "sec", "kg");
        selectSecond.getItems().removeAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra",
                "N", "D", "m^3", "gallon", "pint", "quart", "barrel",
                "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week",
                "Month", "Astronomical Year", "Third", "g", "c", "carat",
                "eng pound", "pound", "stone", "rus pound");
        selectFirst.getItems().addAll("C");
        selectSecond.getItems().addAll("K", "F", "Re", "Ro", "Ra", "N", "D");
    }

    @FXML
    void handleButtonAction3(ActionEvent event) {
        selectFirst.getItems().removeAll("m", "C", "l", "sec", "kg");
        selectSecond.getItems().removeAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra",
                "N", "D", "m^3", "gallon", "pint", "quart", "barrel",
                "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week",
                "Month", "Astronomical Year", "Third", "g", "c", "carat",
                "eng pound", "pound", "stone", "rus pound");
        selectFirst.getItems().addAll("l");
        selectSecond.getItems().addAll("m^3", "gallon", "pint", "quart",
                "barrel", "cubic foot", "cubic inch");
    }

    @FXML
    void handleButtonAction4(ActionEvent event) {
        selectFirst.getItems().removeAll("m", "C", "l", "sec", "kg");
        selectSecond.getItems().removeAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra",
                "N", "D", "m^3", "gallon", "pint", "quart", "barrel",
                "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week",
                "Month", "Astronomical Year", "Third", "g", "c", "carat",
                "eng pound", "pound", "stone", "rus pound");
        selectFirst.getItems().addAll("sec");
        selectSecond.getItems().addAll("Min", "Hour", "Day", "Week",
                "Month", "Astronomical Year", "Third");

    }

    @FXML
    void handleButtonAction5(ActionEvent event) {
        selectFirst.getItems().removeAll("m", "C", "l", "sec", "kg");
        selectSecond.getItems().removeAll("km", "mile", "nautical mile",
                "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra",
                "N", "D", "m^3", "gallon", "pint", "quart", "barrel",
                "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week",
                "Month", "Astronomical Year", "Third", "g", "c", "carat",
                "eng pound", "pound", "stone", "rus pound");
        selectFirst.getItems().addAll("kg");
        selectSecond.getItems().addAll("g", "c", "carat", "eng pound",
                "pound", "stone", "rus pound");

    }

    @FXML
    void handleButtonAction6(ActionEvent event) {
        try {
            boolean firstpol = false;
            boolean secondpol = false;

            if (!textFirstNumber.getText().equals("") &&
                    textSecondNumber.getText().equals("")) {
                secondpol = true;
            } else if (!textSecondNumber.getText().equals("") &&
                    textFirstNumber.getText().equals("")) {

                firstpol = true;
            } else {
                textFirstNumber.setText("");
                textSecondNumber.setText("");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setContentText("Fill in one field");
                alert.showAndWait();
            }
            switch (selectFirst.getValue()) {
                case "m": {
                    switch (selectSecond.getValue()) {
                        case "km": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.mToKm(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.kmToM(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "mile": {
                            if (secondpol) {
                                textSecondNumber.setText(String.valueOf(Exchanger.
                                        mToMile(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.mileToM(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "nautical mile": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.
                                                mToNauticalMile(Double.
                                                        parseDouble(textFirstNumber.
                                                                getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.
                                                nauticalMileToM(Double.
                                                        parseDouble(textSecondNumber.
                                                                getText()))));
                            break;
                        }
                        case "cable": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.mToCable(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.cableToM(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "league": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.mToLeague(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.leagueToM(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "foot": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.mToFoot(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.footToM(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "yard": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.mToYard(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.yardToM(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                    }
                    break;
                }

                case "l": {
                    switch (selectSecond.getValue()) {
                        case "m^3": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToM(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.mToL(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "gallon": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToGallon(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.gallonToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "pint": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToPint(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.pintToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "quart": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToQuart(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.quartToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "barrel": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToBarrel(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.barrelToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "cubic foot": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToCubicFoot(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.cubicFootToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "cubic inch": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.lToCubicInch(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.cubicInchToL(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                    }
                    break;
                }

                case "sec": {
                    switch (selectSecond.getValue()) {
                        case "Min": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToMin(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.minToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "Hour": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToHour(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.hourToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "Day": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToDay(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.dayToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "Week": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToWeek(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.weekToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "Month": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToMonth(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.monthToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                        case "Astronomical Year": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.
                                                secToAstronomicalYear(Double.
                                                        parseDouble(textFirstNumber.
                                                                getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.
                                                astronomicalYearToSec(Double.
                                                        parseDouble(textSecondNumber.
                                                                getText()))));
                            break;
                        }
                        case "Third": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.secToThird(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.thirdToSec(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                    }
                    break;
                }

                case "kg": {
                    switch (selectSecond.getValue()) {
                        case "g": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToG(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.gToKg(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }

                        case "c": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToC(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.cToKg(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }

                        case "carat": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToCarat(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.caratToKg(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }

                        case "eng pound": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToEngPound(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.engPoundToKg(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }

                        case "pound": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToPound(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.poundToKg(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }

                        case "stone": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToStone(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.stoneToKg(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }

                        case "rus pound": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.kgToRusPound(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else
                                textFirstNumber.setText(String.
                                        valueOf(Exchanger.rusPoundToKg(Double.
                                                parseDouble(textSecondNumber.
                                                        getText()))));
                            break;
                        }
                    }
                    break;
                }

                case "C": {
                    switch (selectSecond.getValue()) {
                        case "K": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToK(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.kToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "F": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToF(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.fToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "Re": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToRe(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.reToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "Ro": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToRo(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.roToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "Ra": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToRa(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.raToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "N": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToN(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.nToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                        case "D": {
                            if (secondpol) {
                                textSecondNumber.setText(String.
                                        valueOf(Exchanger.cToD(Double.
                                                parseDouble(textFirstNumber.
                                                        getText()))));
                            } else textFirstNumber.setText(String.
                                    valueOf(Exchanger.dToC(Double.
                                            parseDouble(textSecondNumber.
                                                    getText()))));
                            break;
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setContentText("You fault. Please try again");
            alert.showAndWait();
        }
    }

    @FXML
    void initialize() {

    }
}

