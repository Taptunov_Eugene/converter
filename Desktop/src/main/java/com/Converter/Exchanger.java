package com.Converter;

public class Exchanger {
    public static double mToKm(double num) {

        return num * 0.001;

    }

    public static double mToMile(double num) {

        return num * 0.0006;

    }

    public static double mToNauticalMile(double num) {

        return num * 0.00053;

    }

    public static double mToCable(double num) {

        return num * 0.0054;

    }

    public static double mToLeague(double num) {

        return num * 0.00018;

    }

    public static double mToFoot(double num) {

        return num * 3.28;

    }

    public static double mToYard(double num) {

        return num * 1.09;

    }

    public static double kmToM(double num) {

        return num / 0.001;

    }

    public static double mileToM(double num) {

        return num / 0.0006;

    }

    public static double nauticalMileToM(double num) {

        return num / 0.00054;

    }

    public static double cableToM(double num) {

        return num / 0.0054;

    }

    public static double leagueToM(double num) {

        return num / 0.000179986;

    }

    public static double footToM(double num) {

        return num / 3.28;

    }

    public static double yardToM(double num) {

        return num / 1.09;

    }

    public static double cToK(double num) {

        return num + 273.15;

    }

    public static double cToF(double num) {

        return num * 9 / 5 + 32;

    }

    public static double cToRe(double num) {

        return num * 0.8;

    }

    public static double cToRo(double num) {

        return (num - 7.5) * 40 / 21;

    }

    public static double cToRa(double num) {

        return num * 1.8;

    }

    public static double cToN(double num) {

        return num * 0.33;

    }

    public static double cToD(double num) {

        return num * 1.5 + 100;

    }

    public static double kToC(double num) {

        return num - 273.15;

    }

    public static double fToC(double num) {

        return (num / 9 * 5) - 32;

    }

    public static double reToC(double num) {

        return num / 0.8;

    }

    public static double roToC(double num) {

        return (num + 7.5) / 40 * 21;

    }

    public static double raToC(double num) {

        return num / 1.8;

    }

    public static double nToC(double num) {

        return num / 0.33;

    }

    public static double dToC(double num) {

        return num / 1.5 - 100;

    }

    public static double kgToG(double num) {

        return num * 1000;

    }

    public static double kgToC(double num) {

        return num * 0.01;

    }

    public static double kgToCarat(double num) {

        return num * 500;

    }

    public static double kgToEngPound(double num) {

        return num * 4.4;

    }

    public static double kgToPound(double num) {

        return num * 2.2;

    }

    public static double kgToStone(double num) {

        return num * 0.15;

    }

    public static double kgToRusPound(double num) {

        return num * 2.4;

    }

    public static double gToKg(double num) {

        return num / 1000;

    }

    public static double cToKg(double num) {

        return num / 0.01;

    }

    public static double caratToKg(double num) {

        return num / 500;

    }

    public static double engPoundToKg(double num) {

        return num / 4.4;

    }

    public static double poundToKg(double num) {

        return num / 2.2;

    }

    public static double stoneToKg(double num) {

        return num / 0.15;

    }

    public static double rusPoundToKg(double num) {

        return num / 2.4;

    }

    public static double secToMin(double num) {

        return num / 60;

    }

    public static double secToHour(double num) {

        return num / 3600;

    }

    public static double secToDay(double num) {

        return num / 86400;

    }

    public static double secToWeek(double num) {

        return num / 604800;

    }

    public static double secToMonth(double num) {

        return num / 2628000;

    }

    public static double secToAstronomicalYear(double num) {

        return num / 31540000;

    }

    public static double secToThird(double num) {

        return num / 315400000;

    }

    public static double minToSec(double num) {

        return num * 60;

    }

    public static double hourToSec(double num) {

        return num * 3600;

    }

    public static double dayToSec(double num) {

        return num * 86400;

    }

    public static double weekToSec(double num) {

        return num * 604800;

    }

    public static double monthToSec(double num) {

        return num * 2628000;

    }

    public static double astronomicalYearToSec(double num) {

        return num * 31540000;

    }

    public static double thirdToSec(double num) {

        return num * 315400000;

    }

    public static double lToM(double num) {

        return num / 0.001;

    }

    public static double lToGallon(double num) {

        return num / 0.265;

    }

    public static double lToPint(double num) {

        return num / 2.11;

    }

    public static double lToQuart(double num) {

        return num / 1.056;

    }

    public static double lToBarrel(double num) {

        return num / 0.0085;

    }

    public static double lToCubicFoot(double num) {

        return num / 0.035;

    }

    public static double lToCubicInch(double num) {

        return num / 61.02;

    }

    public static double mToL(double num) {

        return num * 0.001;

    }

    public static double gallonToL(double num) {

        return num * 0.265;

    }

    public static double pintToL(double num) {

        return num * 2.11;

    }

    public static double quartToL(double num) {

        return num * 1.056;

    }

    public static double barrelToL(double num) {

        return num * 0.0085;

    }

    public static double cubicFootToL(double num) {

        return num * 0.035;

    }

    public static double cubicInchToL(double num) {

        return num * 61.02;

    }

}
