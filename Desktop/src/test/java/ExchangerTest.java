import static org.junit.jupiter.api.Assertions.*;

import com.Converter.Exchanger;

import org.junit.jupiter.api.Test;

public class ExchangerTest {

    @Test
    public void mToKmTest() {
        assertEquals(1, Exchanger.mToKm(1000), 0.1);
    }

    @Test
    public void mToMileTest() {
        assertEquals(0.6, Exchanger.mToMile(1000), 0.1);
    }

    @Test
    public void mToNauticalMileTest() {
        assertEquals(0.53, Exchanger.mToNauticalMile(1000), 0.1);
    }

    @Test
    public void mToCableTest() {
        assertEquals(5.4, Exchanger.mToCable(1000), 0.1);
    }

    @Test
    public void mToLeagueTest() {
        assertEquals(0.18, Exchanger.mToLeague(1000), 0.1);
    }

    @Test
    public void mToFootTest() {
        assertEquals(3280, Exchanger.mToFoot(1000), 0.1);
    }

    @Test
    public void mToYardTest() {
        assertEquals(1090, Exchanger.mToYard(1000), 0.1);
    }

    @Test
    public void kmToMTest() {
        assertEquals(1000000, Exchanger.kmToM(1000), 0.1);
    }

    @Test
    public void mileToMTest() {
        assertEquals(1666666.67, Exchanger.mileToM(1000), 0.1);
    }

    @Test
    public void nauticalMileToMTest() {
        assertEquals(1851851.85, Exchanger.nauticalMileToM(1000), 0.1);
    }

    @Test
    public void cableToMTest() {
        assertEquals(185185.185, Exchanger.cableToM(1000), 0.1);
    }

    @Test
    public void footToMTest() {
        assertEquals(304.87, Exchanger.footToM(1000), 0.1);
    }

    @Test
    public void yardToMTest() {
        assertEquals(917.43, Exchanger.yardToM(1000), 0.1);
    }

    @Test
    public void cToKTest() {
        assertEquals(1273.15, Exchanger.cToK(1000), 0.1);
    }

    @Test
    public void cToFTest() {
        assertEquals(1832, Exchanger.cToF(1000), 0.1);
    }

    @Test
    public void cToReTest() {
        assertEquals(800, Exchanger.cToRe(1000), 0.1);
    }

    @Test
    public void cToRoTest() {
        assertEquals(1890.47, Exchanger.cToRo(1000), 0.1);
    }

    @Test
    public void cToRaTest() {
        assertEquals(1800, Exchanger.cToRa(1000), 0.1);
    }

    @Test
    public void cToNTest() {
        assertEquals(330, Exchanger.cToN(1000), 0.1);
    }

    @Test
    public void cToDTest() {
        assertEquals(1600, Exchanger.cToD(1000), 0.1);
    }

    @Test
    public void kToCTest() {
        assertEquals(726.85, Exchanger.kToC(1000), 0.1);
    }

    @Test
    public void fToCTest() {
        assertEquals(523.55, Exchanger.fToC(1000), 0.1);
    }

    @Test
    public void reToCTest() {
        assertEquals(1250, Exchanger.reToC(1000), 0.1);
    }

    @Test
    public void roToCTest() {
        assertEquals(528.93, Exchanger.roToC(1000), 0.1);
    }

    @Test
    public void raToCTest() {
        assertEquals(555.555, Exchanger.raToC(1000), 0.1);
    }

    @Test
    public void nToCTest() {
        assertEquals(3030.3, Exchanger.nToC(1000), 0.1);
    }

    @Test
    public void dToCTest() {
        assertEquals(566.66, Exchanger.dToC(1000), 0.1);
    }

    @Test
    public void kgToGTest() {
        assertEquals(1000000, Exchanger.kgToG(1000), 0.1);
    }

    @Test
    public void kgToCTest() {
        assertEquals(10, Exchanger.kgToC(1000), 0.1);
    }

    @Test
    public void kgToCaratTest() {
        assertEquals(500000, Exchanger.kgToCarat(1000), 0.1);
    }

    @Test
    public void kgToEngPoundTest() {
        assertEquals(4400, Exchanger.kgToEngPound(1000), 0.1);
    }

    @Test
    public void kgToPoundTest() {
        assertEquals(2200, Exchanger.kgToPound(1000), 0.1);
    }

    @Test
    public void kgToStoneTest() {
        assertEquals(150, Exchanger.kgToStone(1000), 0.1);
    }

    @Test
    public void kgToRusPoundTest() {
        assertEquals(2400, Exchanger.kgToRusPound(1000), 0.1);
    }

    @Test
    public void gToKgTest() {
        assertEquals(1, Exchanger.gToKg(1000), 0.1);
    }

    @Test
    public void cToKgTest() {
        assertEquals(100000, Exchanger.cToKg(1000), 0.1);
    }

    @Test
    public void caratToKgTest() {
        assertEquals(2, Exchanger.caratToKg(1000), 0.1);
    }

    @Test
    public void engPoundToKgTest() {
        assertEquals(227.27, Exchanger.engPoundToKg(1000), 0.1);
    }

    @Test
    public void poundToKgTest() {
        assertEquals(454.454, Exchanger.poundToKg(1000), 0.1);
    }

    @Test
    public void stoneToKgTest() {
        assertEquals(6666.66, Exchanger.stoneToKg(1000), 0.1);
    }

    @Test
    public void rusPoundToKgTest() {
        assertEquals(416.66, Exchanger.rusPoundToKg(1000), 0.1);
    }

    @Test
    public void secToMinTest() {
        assertEquals(16.6, Exchanger.secToMin(1000), 0.1);
    }

    @Test
    public void secToHourTest() {
        assertEquals(0.27, Exchanger.secToHour(1000), 0.1);
    }

    @Test
    public void secToDayTest() {
        assertEquals(0.011, Exchanger.secToDay(1000), 0.1);
    }

    @Test
    public void secToWeekTest() {
        assertEquals(0.001, Exchanger.secToWeek(1000), 0.1);
    }

    @Test
    public void secToMonthTest() {
        assertEquals(0.003, Exchanger.secToMonth(1000), 0.1);
    }

    @Test
    public void secToAstronomicalYearTest() {
        assertEquals(0.00003, Exchanger.secToAstronomicalYear(1000), 0.1);
    }

    @Test
    public void secToThirdTest() {
        assertEquals(0.00009, Exchanger.secToThird(1000), 0.1);
    }

    @Test
    public void minToSecTest() {
        assertEquals(60000, Exchanger.minToSec(1000), 0.1);
    }

    @Test
    public void hourToSecTest() {
        assertEquals(3600000, Exchanger.hourToSec(1000), 0.1);
    }

    @Test
    public void dayToSecTest() {
        assertEquals(86400000, Exchanger.dayToSec(1000), 0.1);
    }

    @Test
    public void weekToSecTest() {
        assertEquals(60480000, Exchanger.weekToSec(100), 0.1);
    }

    @Test
    public void monthToSecTest() {
        assertEquals(262800000, Exchanger.monthToSec(100), 0.1);
    }

    @Test
    public void astronomicalYearToSecTest() {
        assertEquals(315400000, Exchanger.astronomicalYearToSec(10), 0.1);
    }

    @Test
    public void thirdToSecTest() {
        assertEquals(3.154E11, Exchanger.thirdToSec(1000), 0.1);
    }

    @Test
    public void lToMTest() {
        assertEquals(1000000, Exchanger.lToM(1000), 0.1);
    }

    @Test
    public void lToGallonTest() {
        assertEquals(3773.58, Exchanger.lToGallon(1000), 0.1);
    }

    @Test
    public void lToPintTest() {
        assertEquals(473.93, Exchanger.lToPint(1000), 0.1);
    }

    @Test
    public void lToQuartTest() {
        assertEquals(946.96, Exchanger.lToQuart(1000), 0.1);
    }

    @Test
    public void lToBarrelTest() {
        assertEquals(117647, Exchanger.lToBarrel(1000), 0.1);
    }

    @Test
    public void lToCubicFootTest() {
        assertEquals(28571.42, Exchanger.lToCubicFoot(1000), 0.1);
    }

    @Test
    public void lToCubicInchTest() {
        assertEquals(16.38, Exchanger.lToCubicInch(1000), 0.1);
    }

    @Test
    public void mToLTest() {
        assertEquals(1, Exchanger.mToL(1000), 0.1);
    }

    @Test
    public void gallonToLTest() {
        assertEquals(265, Exchanger.gallonToL(1000), 0.1);
    }

    @Test
    public void pintToLTest() {
        assertEquals(2110, Exchanger.pintToL(1000), 0.1);
    }

    @Test
    public void quartToLTest() {
        assertEquals(1056, Exchanger.quartToL(1000), 0.1);
    }

    @Test
    public void barrelToLTest() {
        assertEquals(8.5, Exchanger.barrelToL(1000), 0.1);
    }

    @Test
    public void cubicFootToLTest() {
        assertEquals(35, Exchanger.cubicFootToL(1000), 0.1);
    }

    @Test
    public void cubicInchToLTest() {
        assertEquals(61020, Exchanger.cubicInchToL(1000), 0.1);
    }
}